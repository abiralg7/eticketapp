﻿using eTicketBooking.Data;
using eTicketBooking.Data.Services;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace eTicketBooking.Controllers
{
    public class ActorsController : Controller
    {
        private readonly IActorsService _service;
        private readonly AppDbContext _context;
        public ActorsController(IActorsService service, AppDbContext context)
        {
            _service = service;
            _context = context;
        }
       
        //GET ALL
        public async Task<IActionResult> Index()
        {
            var data = await _service.GetAllAsync();
            return View(data);
        }

        public async Task<IActionResult> Users()
        {
            var users = await _context.Users.ToListAsync();
            return View(users);
        }

        //Get: Action/Create
        public IActionResult Create() 
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("FullName,ProfilePicture,Bio")]Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return View(actor);
            }
            string uniqueFileName = _service.UploadedFile(actor);
            actor.ProfilePictureURL = uniqueFileName;
            await _service.AddAsync(actor);
            return RedirectToAction(nameof(Index));
        }

        //Get Actors/Details/id
        public async Task<IActionResult> Details(int id)
        {
            var actorsDetails = await _service.GetByIdAsync(id);
            if (actorsDetails == null)
            {
                return View("Empty");
            }
            return View(actorsDetails);
        }

        //Get: Action/Update/1
        public async Task<IActionResult> Edit(int id)
        {
            var actorsDetails = await _service.GetByIdAsync(id);

            if (actorsDetails == null)
            {
                return View("Not Found");
            }

            return View(actorsDetails);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FullName,ProfilePicture,Bio")] Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return View(actor);
            }
            string uniqueFileName = _service.UploadedFile(actor);
            actor.ProfilePictureURL = uniqueFileName;
            await _service.UpdateAsync(id, actor);
            return RedirectToAction(nameof(Index));
        }

        //Get: Action/Delete/1
        public async Task<IActionResult> Delete(int id)
        {
            var actorsDetails = await _service.GetByIdAsync(id);

            if (actorsDetails == null)
            {
                return View("Not Found");
            }

            return View(actorsDetails);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var actorsDetails = await _service.GetByIdAsync(id);
            if (actorsDetails == null) return View("Not Found");
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));

        }
    }
}
