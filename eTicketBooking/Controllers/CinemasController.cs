﻿using eTicketBooking.Data;
using eTicketBooking.Data.Services;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Controllers
{
    public class CinemasController : Controller
    {
        private readonly ICinemaService _service;
        public CinemasController(ICinemaService service)
        {
            _service = service;
        }
        public async Task<ActionResult> Index()
        {
            var allCinemas = await _service.GetAllAsync();
            return View(allCinemas);
        }
        //Get: Action/Create
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Logo,Name,Description")]Cinema cinema)
        {
            if (!ModelState.IsValid)
            {
                return View(cinema);
            }
            string uniqueFileName = _service.UploadedFile(cinema);
            cinema.LogoURL = uniqueFileName;
            await _service.AddAsync(cinema);
            return RedirectToAction(nameof(Index));

        }
        //Get Details/id
        public async Task<IActionResult> Details(int id)
        {
            var cinemaDetails = await _service.GetByIdAsync(id);
            if(cinemaDetails == null)
            {
                return View("Not Found");
            }
            return View(cinemaDetails);
        }
        //Get Edit/id
        public async Task<IActionResult> Edit(int id)
        {
            var cinemaDetails = await _service.GetByIdAsync(id);
            if(cinemaDetails == null)
            {
                return View("Not Found");
            }
            return View(cinemaDetails);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id,[Bind("Id,Logo,Name,Description")] Cinema cinema)
        {
            if (!ModelState.IsValid)
            {
                return View(cinema);
            }
            string uniqueFileName = _service.UploadedFile(cinema);
            cinema.LogoURL = uniqueFileName;
            await _service.UpdateAsync(id, cinema);
            return RedirectToAction(nameof(Index));
        }
        //Get Delete/id
        public async Task<IActionResult> Delete(int id)
        {
            var cinemaDetails = await _service.GetByIdAsync(id);
            if(cinemaDetails == null)
            {
                return View("Not Found");
            }
            return View(cinemaDetails);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
