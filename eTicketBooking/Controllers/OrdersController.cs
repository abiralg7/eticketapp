﻿using eTicketBooking.Data.Cart;
using eTicketBooking.Data.Services;
using eTicketBooking.Data.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace eTicketBooking.Controllers
{

    public class OrdersController : Controller
    {
        private readonly IMoviesService _moviesService;
        private readonly ShoppingCart _shoppingCart;
        private readonly IOrdersService _service;
        public OrdersController(IMoviesService moviesService, ShoppingCart shoppingCart, IOrdersService service)
        {
            _moviesService = moviesService;
            _shoppingCart = shoppingCart;
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userRole = User.FindFirstValue(ClaimTypes.Role);
            var orders = await _service.GetOrdersByUserIdAndRoleAsync(userId, userRole);
            return View(orders);
            
        }
        public IActionResult ShoppingCart()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;
            var response = new ShoppingCartVM()
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };
            return View(response);
        }
        public async Task<IActionResult> AddItemToShoppingCart(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var item = await _moviesService.GetMovieByIdAsync(id);
                if (item != null)
                {
                    _shoppingCart.AddItemToCart(item);
                }
                return RedirectToAction(nameof(ShoppingCart));
            }
            TempData["Error"] = "Please signin first";
            return RedirectToAction("Login", "Account");
        }
        public async Task<IActionResult> RemoveItemFromShoppingCart(int id)
        {
            var item = await _moviesService.GetMovieByIdAsync(id);
            if (item != null)
            {
                _shoppingCart.RemoveitemFromCart(item);
            }
            return RedirectToAction(nameof(ShoppingCart));
        }
         public async Task<IActionResult> CompleteOrder()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userEmailAddress = User.FindFirstValue(ClaimTypes.Email);

            await _service.StoreOrderAsync(items,userId,userEmailAddress);
            await _shoppingCart.ClearShoppingCartAsync();
            return View("OrderCompleted");
        }

    }
}
