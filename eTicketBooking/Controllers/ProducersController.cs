﻿using eTicketBooking.Data;
using eTicketBooking.Data.Services;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Controllers
{
    public class ProducersController : Controller
    {
        private readonly IProducersService _service;
        public ProducersController(IProducersService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var allProducer = await _service.GetAllAsync();
            return View(allProducer);
        }
        //Show details Producers/1
        public async Task<IActionResult> Details(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return View(result);
        }
        //Create Producers/1
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]//Post method to create Producer
        public async Task<IActionResult> Create([Bind("FullName,ProfilePicture,Bio")] Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return View(producer);
            }
            var uniqueFileName = _service.UploadedFile(producer);
            producer.ProfilePictureURL = uniqueFileName;
            await _service.AddAsync(producer);
            return RedirectToAction(nameof(Index));
        }
        //Edit Producers/1
        public async Task<IActionResult> Edit(int id)
        {
            var result = await _service.GetByIdAsync(id);
            if(result == null)
            {
                return View("Not Found");
            }
            return View(result);
        }
        [HttpPost]//Post method to Edit Producer
        public async Task<IActionResult> Edit(int id, Producer producer)
        {
            if (!ModelState.IsValid)
            {
                return View(producer);
            }
            var uniqueFileName = _service.UploadedFile(producer);
            producer.ProfilePictureURL = uniqueFileName;
            await _service.UpdateAsync(id,producer);
            return RedirectToAction(nameof(Index));
        }
        //Edit Producers/1
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.GetByIdAsync(id);
            if (result == null)
            {
                return View("Not Found");
            }
            return View(result);
        }
        [HttpPost]//Post method to Edit Producer
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var result = await _service.GetByIdAsync(id);
            if(result == null)
            {
                return View("Not Found");
            }
           
            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
