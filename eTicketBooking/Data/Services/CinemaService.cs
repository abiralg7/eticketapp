﻿using eTicketBooking.Data.Base;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Services
{
    public class CinemaService : EntityBaseRepository<Cinema>, ICinemaService
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public CinemaService(AppDbContext context, IWebHostEnvironment webHostEnvironment) : base(context)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public string UploadedFile(Cinema cinema)
        {
            string uniqueFileName = null;
            if (cinema.Logo != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/cinemas");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + cinema.Logo.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                   cinema.Logo.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
    }
}
