﻿using eTicketBooking.Data.Base;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Services
{
    public class ProducerService : EntityBaseRepository<Producer>, IProducersService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        public ProducerService(AppDbContext context, IWebHostEnvironment webHostEnvironment) : base(context)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public string UploadedFile(Producer producer)
        {
            string uniqueFileName = null;
            if(producer != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/producers");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + producer.ProfilePicture.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath,FileMode.Create))
                {
                    producer.ProfilePicture.CopyTo(fileStream);
                }

            }
            return uniqueFileName;
        }
    }
}
