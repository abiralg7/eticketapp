﻿using eTicketBooking.Data.Base;
using eTicketBooking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Services
{
    public interface ICinemaService : IEntityBaseRepository<Cinema>
    {
        string UploadedFile(Cinema cinema);
    }
}
