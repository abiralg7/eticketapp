﻿using eTicketBooking.Data.Base;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Services
{
    public class ActorsService :EntityBaseRepository<Actor>, IActorsService
    {
        private readonly AppDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ActorsService(AppDbContext context, IWebHostEnvironment webHostEnvironment) : base(context) 
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }
        public string UploadedFile(Actor actor)
        {
            string uniqueFileName = null;
            if (actor.ProfilePicture != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images/actors");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + actor.ProfilePicture.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    actor.ProfilePicture.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

       
    }
}
