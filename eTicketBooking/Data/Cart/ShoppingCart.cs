﻿using eTicketBooking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Cart
{
    public class ShoppingCart
    {
        public AppDbContext _context { get; set; }
        public string ShoppingCartId { get; set; }
        public List<ShoppingCartItem> ShoppingCartItems { get; set; }
        public ShoppingCart(AppDbContext context)
        {
            _context = context;
        }
        public static ShoppingCart GetShoppingCart(IServiceProvider services)
        {
            ISession serssion = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var context = services.GetService<AppDbContext>();

            string cartId = serssion.GetString("CartId") ?? Guid.NewGuid().ToString();
            serssion.SetString("CartId", cartId);

            return new ShoppingCart(context) { ShoppingCartId = cartId };
        }
        
        public void AddItemToCart(Movie movie)
        {
            var shoppingItem = _context.ShoppingCartItems.FirstOrDefault(n => n.Movie.Id == movie.Id &&
            n.ShoppingCartId == ShoppingCartId);  
            if(shoppingItem == null)
            {
                shoppingItem = new ShoppingCartItem()
                {
                    ShoppingCartId = ShoppingCartId,
                    Movie = movie,
                    Amount = 1
                };
                _context.ShoppingCartItems.Add(shoppingItem);
            }
            else
            {
                shoppingItem.Amount++;
            }
            _context.SaveChanges();
        }
        public void RemoveitemFromCart(Movie movie)
        {
            var shoppingItem = _context.ShoppingCartItems.FirstOrDefault(n => n.Movie.Id == movie.Id &&
           n.ShoppingCartId == ShoppingCartId);
            if (shoppingItem != null)
            {
                if(shoppingItem.Amount > 1)
                {
                    shoppingItem.Amount--;
                }
                else
                {
                    _context.ShoppingCartItems.Remove(shoppingItem);
                }
            }
            _context.SaveChanges();
        }
        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return ShoppingCartItems ?? (ShoppingCartItems = _context.ShoppingCartItems
                .Where(n => n.ShoppingCartId == ShoppingCartId)
                .Include(n => n.Movie).ToList());
        }
        public double GetShoppingCartTotal()
        {

            var total = _context.ShoppingCartItems.Where(n => n.ShoppingCartId == ShoppingCartId)
                .Select(n => n.Movie.Price * n.Amount).Sum();
            return total;
        }
        public async Task ClearShoppingCartAsync()
        {
            var items = await _context.ShoppingCartItems
                .Where(n => n.ShoppingCartId == ShoppingCartId).ToListAsync();
            _context.ShoppingCartItems.RemoveRange(items);
            await _context.SaveChangesAsync();
        }


    }
}
