﻿using eTicketBooking.Data.Static;
using eTicketBooking.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data
{
    public class AppDbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<AppDbContext>();

                context.Database.EnsureCreated();

                //Cinema
                if (!context.Cinemas.Any())
                {
                    context.Cinemas.AddRange(new List<Cinema>()
                    {
                        new Cinema()
                        {
                            Name = "Alamo Drafthouse",
                            LogoURL = "16ba9ccb-d897-4832-965a-cf4b1b79ac0c_Alamo Drafthouse.jpg",
                            Description = "This is the description of the first cinema"
                        },
                        new Cinema()
                        {
                            Name = "Eglinton Theatre",
                            LogoURL = "1a66810f-31a6-467b-a919-4f23b5a818aa_Eglinton Theatre.jpg",
                            Description = "This is the description of the first cinema"
                        },
                        new Cinema()
                        {
                            Name = "Teatro Adriano",
                            LogoURL = "d8c9d881-aaf0-434b-920a-64306cfc52cd_Teatro Adriano.jfif",
                            Description = "This is the description of the first cinema"
                        },
                        new Cinema()
                        {
                            Name = "Royal Theater Heerlen",
                            LogoURL = "46be292c-8433-4652-80d1-cd9ed2d0b61e_Royal Theater Heerlen.JPG",
                            Description = "This is the description of the first cinema"
                        },
                        new Cinema()
                        {
                            Name = "Maximteatern",
                            LogoURL = "e36b8f46-16b2-479e-b40b-76ce8140eb84_Maximteatern.jpg",
                            Description = "This is the description of the first cinema"
                        },
                    });
                    context.SaveChanges();
                }
                //Actors
                if (!context.Actors.Any())
                {
                    context.Actors.AddRange(new List<Actor>()
                    {
                        new Actor()
                        {
                            FullName = "Dwayne Johnson",
                            Bio = "This is the Bio of the first actor",
                            ProfilePictureURL = "e81316ed-48f8-402f-851d-874982ad4fc7_Dj.jpg"

                        },
                        new Actor()
                        {
                            FullName = "Robert Downey Jr",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "1018089a-29c3-414b-9be7-5bf904a65011_Jr.jpg"
                        },
                        new Actor()
                        {
                            FullName = "Jhonny Depp",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "3f8c7a64-bffe-4a31-91df-871d82c22277_Jhony Depp.jpg"
                        },
                        new Actor()
                        {
                            FullName = "Emma Watson",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "b3db4c5b-34c5-42e3-bb18-f873730b2dd3_Emma Watson.jpg"
                        },
                        new Actor()
                        {
                            FullName = "Sacrlet Johanson",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "45943051-d5ee-4a2e-961d-05e3c6e83645_Scarlet.jpg"
                        }
                    });
                    context.SaveChanges();
                }
                //Producers
                if (!context.Producers.Any())
                {
                    context.Producers.AddRange(new List<Producer>()
                    {
                        new Producer()
                        {
                            FullName = "Steven Spielberg",
                            Bio = "This is the Bio of the first actor",
                            ProfilePictureURL = "5b89c273-3cf5-4260-bf76-fca80179cc41_Steven Spielberg.jpg"

                        },
                        new Producer()
                        {
                            FullName = "Spike Lee",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "dd2dd3d1-d6d1-4876-b55d-0eae0ddee8b1_Spike Lee.jpg"
                        },
                        new Producer()
                        {
                            FullName = "Quentin Tarantino",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "1ca15f68-ba3e-4c21-b8eb-044436927944_Quentin Tarantino.jpg"
                        },
                        new Producer()
                        {
                            FullName = "Irwin Winkler",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "c29eff4c-faf4-492d-8dbe-433c09acb6b8_Irwin Winkler.jpg"
                        },
                        new Producer()
                        {
                            FullName = "Nina Jacobson",
                            Bio = "This is the Bio of the second actor",
                            ProfilePictureURL = "04d2968e-2165-4408-ab5f-d2ec856ffc4c_nina.jpg"
                        }
                    });
                    context.SaveChanges();
                }
                //Movies
                if (!context.Movies.Any())
                {
                    context.Movies.AddRange(new List<Movie>()
                    {
                        new Movie()
                        {
                            Name = "Venom",
                            Description = "This is the Life movie description",
                            Price = 39.50,
                            ImageURL = "7d1e04d5-c9d9-4583-9063-1bfe3035d96f_Venom.jpg",
                            StartDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now.AddDays(10),
                            CinemaId = 3,
                            ProducerId = 3,
                            MovieCategory = MovieCategory.Action
                        },
                        new Movie()
                        {
                            Name = "The Shawshank Redemption",
                            Description = "This is the Shawshank Redemption description",
                            Price = 29.50,
                            ImageURL = "59a88393-7bfc-4239-89ad-973debfc295a_The Shawshank Redemption.jpg",
                            StartDate = DateTime.Now,
                            EndDate = DateTime.Now.AddDays(3),
                            CinemaId = 1,
                            ProducerId = 1,
                            MovieCategory = MovieCategory.Action
                        },
                        new Movie()
                        {
                            Name = "Scream",
                            Description = "This is the Ghost movie description",
                            Price = 39.50,
                            ImageURL = "7091c36f-3203-4c5a-a779-02568e2eca50_Scream.jpg",
                            StartDate = DateTime.Now,
                            EndDate = DateTime.Now.AddDays(7),
                            CinemaId = 4,
                            ProducerId = 4,
                            MovieCategory = MovieCategory.Horror
                        },
                        new Movie()
                        {
                            Name = "14 Peaks",
                            Description = "This is the Race movie description",
                            Price = 39.50,
                            ImageURL = "536c5167-93cd-4592-bc85-34ccf08d6162_14Peaks.jpg",
                            StartDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now.AddDays(-5),
                            CinemaId = 1,
                            ProducerId = 2,
                            MovieCategory = MovieCategory.Documentary
                        },
                        new Movie()
                        {
                            Name = "Ratatouille",
                            Description = "This is the Scoob movie description",
                            Price = 39.50,
                            ImageURL = "9f043e14-0ad5-4ed0-8c1c-4fb8e986db4c_Ratatouille.jpg",
                            StartDate = DateTime.Now.AddDays(-10),
                            EndDate = DateTime.Now.AddDays(-2),
                            CinemaId = 1,
                            ProducerId = 3,
                            MovieCategory = MovieCategory.Cartoon
                        },
                        new Movie()
                        {
                            Name = "Jungle Cruise",
                            Description = "This is the Cold Soles movie description",
                            Price = 39.50,
                            ImageURL = "5f65391d-1dde-47d6-baed-e3b418b7a6fd_JungleCruise.jpg",
                            StartDate = DateTime.Now.AddDays(3),
                            EndDate = DateTime.Now.AddDays(20),
                            CinemaId = 1,
                            ProducerId = 5,
                            MovieCategory = MovieCategory.Drama
                        }
                    });
                    context.SaveChanges();
                }
                //Actors & Movies
                if (!context.Actors_Movies.Any())
                {
                    context.Actors_Movies.AddRange(new List<Actor_Movie>()
                    {
                        new Actor_Movie()
                        {
                            ActorId = 1,
                            MovieId = 1
                        },
                        new Actor_Movie()
                        {
                            ActorId = 3,
                            MovieId = 1
                        },

                         new Actor_Movie()
                        {
                            ActorId = 1,
                            MovieId = 2
                        },
                         new Actor_Movie()
                        {
                            ActorId = 4,
                            MovieId = 2
                        },

                        new Actor_Movie()
                        {
                            ActorId = 1,
                            MovieId = 3
                        },
                        new Actor_Movie()
                        {
                            ActorId = 2,
                            MovieId = 3
                        },
                        new Actor_Movie()
                        {
                            ActorId = 5,
                            MovieId = 3
                        },


                        new Actor_Movie()
                        {
                            ActorId = 2,
                            MovieId = 4
                        },
                        new Actor_Movie()
                        {
                            ActorId = 3,
                            MovieId = 4
                        },
                        new Actor_Movie()
                        {
                            ActorId = 4,
                            MovieId = 4
                        },


                        new Actor_Movie()
                        {
                            ActorId = 2,
                            MovieId = 5
                        },
                        new Actor_Movie()
                        {
                            ActorId = 3,
                            MovieId = 5
                        },
                        new Actor_Movie()
                        {
                            ActorId = 4,
                            MovieId = 5
                        },
                        new Actor_Movie()
                        {
                            ActorId = 5,
                            MovieId = 5
                        },


                        new Actor_Movie()
                        {
                            ActorId = 3,
                            MovieId = 6
                        },
                        new Actor_Movie()
                        {
                            ActorId = 4,
                            MovieId = 6
                        },
                        new Actor_Movie()
                        {
                            ActorId = 5,
                            MovieId = 6
                        },
                    });
                    context.SaveChanges();
                }
            }

           

        }

        public static async Task SeedUsersAndRolesAsync(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {

                //Roles Section
                var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                if (!await roleManager.RoleExistsAsync(UserRoles.Admin))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
                if (!await roleManager.RoleExistsAsync(UserRoles.User))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.User));

                //Users
                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                var adminUserEmail = "admin@etickets.com";
                var adminUser = await userManager.FindByEmailAsync(adminUserEmail);
                if(adminUser == null)
                {
                    var newAdminUser = new ApplicationUser()
                    {
                        FullName = "Admin User",
                        UserName = "app-admin",
                        Email = adminUserEmail,
                        EmailConfirmed = true
                    };
                    await userManager.CreateAsync(newAdminUser, "Helloworld1234?");
                    await userManager.AddToRoleAsync(newAdminUser, UserRoles.Admin);
                }


                var appUserEmail = "user@etickets.com";
                var appUser = await userManager.FindByEmailAsync(appUserEmail);

                if (appUser == null)
                {
                    var newAppUser = new ApplicationUser()
                    {
                        FullName = "App User",
                        UserName = "app-user",
                        Email = appUserEmail,
                        EmailConfirmed = true
                    };
                    await userManager.CreateAsync(newAppUser, "Helloworld1234?");
                    await userManager.AddToRoleAsync(newAppUser, UserRoles.User);
                }

            }
        }
    }
}
