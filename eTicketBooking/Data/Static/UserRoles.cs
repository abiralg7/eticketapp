﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Data.Static
{
    public class UserRoles
    {
        public static string Admin = "Admin";
        public static string User = "User";
    }
}
