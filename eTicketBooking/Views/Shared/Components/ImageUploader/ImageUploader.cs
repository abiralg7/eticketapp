﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eTicketBooking.Views.Shared.Components.ImageUploader
{
    public class ImageUploader: ViewComponent
    {
        public ImageUploader()
        {

        }
        public  IViewComponentResult Invoke(string FieldName)
        {
            return View("ImageUploader", FieldName);
        }
    }
}
